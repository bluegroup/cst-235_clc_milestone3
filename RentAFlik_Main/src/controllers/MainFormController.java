package controllers;

import java.io.IOException;
import java.util.Map;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.EnterpriseRegistration;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class MainFormController extends HttpServlet {

	EnterpriseRegistration enterpriseRegistration;
	public MainFormController() {
		System.out.println("HELLO");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("HELLO IT'S ME");
		String userName = request.getParameter("userName");
		String emailAddress = request.getParameter("emailAddress");
		String password = request.getParameter("password");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		
		request.getRequestDispatcher("/MainForm.xhtml").forward(request, response);
	}
	/*public void getData(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("userName");
		System.out.println(userName);
	}*/
}
