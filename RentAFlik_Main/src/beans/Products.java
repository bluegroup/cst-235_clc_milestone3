package beans;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.*;

@ManagedBean(name="productss", eager = true) 
@SessionScoped
public class Products {
	String title;
	String genre;
	String leadAct;
	String studio;
	String director;
	int length;
	int year;
	double price;
	String vidImg;
	private List<Product> productss = new ArrayList<>();
	
    /**
     * Default constructor. 
     */
    public Products() {
    	productss.add(new Product("Funny","Comedy","Billy Bob","Marvel", "Timmy", 120, 2006, 3.50, "ComingSoon.PNG"));
    	productss.add(new Product("Love","Romance","Sally","Fox", "John", 110, 2018, 1.50, "ComingSoon.PNG"));
    }
	public Products(String title, String genre, String leadAct, String studio, String director,
			int length, int year, double price, String vidImg) {
		this.title = title;
		this.genre = genre;
		this.leadAct = leadAct;
		this.studio = studio;
		this.director = director;
		this.length = length;
		this.year = year;
		this.price = price;
		this.vidImg = vidImg;
		
	}
	
	//Method adds the project object from the form to a product list. This list will be used to build the form table
	public String addProd(Product prod){
        Product product = new Product(prod.getTitle(),prod.getGenre(),prod.getLeadAct(),prod.getStudio(),prod.getDirector(),prod.getLength(),prod.getYear(),prod.getPrice(),prod.getVidImg());
        productss.add(product);
        System.out.println(prod.getTitle());
        System.out.println(prod.getGenre());
        return "added";
    }
    
	//This method returns the product list
	public List<Product> getProductss() {
		return productss;
	}
	
	
	
}
