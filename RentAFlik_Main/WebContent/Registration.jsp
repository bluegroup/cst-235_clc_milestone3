<pre><%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:h="http://java.sun.com/jsf/html">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register an account:</title>
<script> 
function validate()
{
 // Necessary variables for registration except for phone which is optional.
 var nickname = document.form.nickname.value;
 var emailaddress = document.form.emailaddress.value;
 var password = document.form.password.value;
 var confirmpassword= document.form.confirmpassword.value;
 var firstname = document.form.firstname.value;
 var lastname = document.form.lastname.value;
 var phone = document.form.phone.value;

 // If a variable is empty, the user is warned.
 if (nickname == "" || nickname == null)
 { 
 alert("Please enter a nickname."); 
 return false; 
 }
 else if (emailaddress == "" || emailaddress == null)
 { 
 alert("Please enter an email address.");
 return false; 
 }
 else if(password.length <= 7) // Password strength feature.
 { 
 alert("Make sure your password is 8 characters long or more."); 
 return false; 
 } 
 else if (confirmpassword != password)
 { 
 alert("Passwords do not match."); 
 return false; 
 }
 else if (firstname == "" || firstname == null)
 { 
 alert("Please enter a first name."); 
 return false;
 }
 else if (lastname == "" || lastname == null)
 { 
 alert("Please enter a last name.");
 return false; 
 }
 }

/* Left-aligned fonts, heading 1 used for "Register an account"Array
and arial used for most text in blue background. Buttons needed. */
</script> 
</head>
<ui:composition template = "/layouts/commonLayout.xhtml">	
	<ui:define name = "commonContent">
<body>
<h1 align = "left">Register an account:</h1>
<BODY BGCOLOR="#AACBCC">
<form name="form" action="RegisterServlet" method="post" onsubmit="return validate()">
<table align="left">
 <tr>
 <td><p style = "font-family:arial;font-size:14px;font-style:bold;">Nickname:</p></td>
 <td><input type="text" name="nickname" /></td>
 </tr>
 <tr>
 <td><p style = "font-family:arial;font-size:14px;font-style:bold;">Email address:</p></td>
 <td><input type="text" name="emailaddress" /></td>
 </tr>
 <td><p style = "font-family:arial;font-size:14px;font-style:bold;">Password:</p></td>
 <td><input type="password" name="password" /></td>
 </tr>
 <tr>
 <td><p style = "font-family:arial;font-size:14px;font-style:bold;">Password confirmation:</p></td>
 <td><input type="password" name="confirmpassword" /></td>
 </tr>
 <tr>
 <td><p style = "font-family:arial;font-size:14px;font-style:bold;">First name:</p></td>
 <td><input type="text" name="firstname" /></td>
 </tr>
 <tr>
 <td><p style = "font-family:arial;font-size:14px;font-style:bold;">Last name:</p></td>
 <td><input type="text" name="lastname" /></td>
 </tr>
 <tr>
 <td><p style = "font-family:arial;font-size:14px;font-style:bold;">Phone number (optional):</p></td>
 <td><input type="text" name="phone" /></td>
 </tr>
 <tr>
 <td><%=(request.getAttribute("errMessage") == null) ? ""
 : request.getAttribute("errMessage")%></td>
 </tr>
 <tr>
 <td></td>
 <td><input type="submit" value="Submit"></input><input
 type="reset" value="Clear"></input></td>
 </tr>
</table>
</form>
</body>
	</ui:define>	
	</ui:composition> 
</html>