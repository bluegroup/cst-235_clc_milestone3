<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Registration Form</title>
</head>
<body>
<script>
function validate()
{
 // Necessary variables for registration except for phone which is optional.
 var nickname = document.form.nickname.value;
 var emailaddress = document.form.emailaddress.value;
 var password = document.form.password.value;
 var confirmpassword= document.form.confirmpassword.value;
 var firstname = document.form.firstname.value;
 var lastname = document.form.lastname.value;
 var phone = document.form.phone.value;

 // If a variable is empty, the user is warned.
 if (nickname == "" || nickname == null)
 { 
 alert("Please enter a nickname."); 
 return false; 
 }
 else if (emailaddress == "" || emailaddress == null)
 { 
 alert("Please enter an email address.");
 return false; 
 }
 else if(password.length <= 7) // Password strength feature.
 { 
 alert("Make sure your password is 8 characters long or more."); 
 return false; 
 } 
 else if (confirmpassword != password)
 { 
 alert("Passwords do not match."); 
 return false; 
 }
 else if (firstname == "" || firstname == null)
 { 
 alert("Please enter a first name."); 
 return false;
 }
 else if (lastname == "" || lastname == null)
 { 
 alert("Please enter a last name.");
 return false; 
 }
 }
 </script>
</body>
</html>